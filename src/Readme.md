## 运行在容器上
#### 1.拉取代码
git clone https://gitee.com/opensourceway/om-data

####2.打包
复制Dockerfile、jenkins-agent到om-data文件夹平级目录
docker build -t test01 .

####3.上传
docker tag test01 swr.cn-north-4.myhuaweicloud.com/om/om-data:0.0.2  
docker push swr.cn-north-4.myhuaweicloud.com/om/om-data:0.0.2  
若上传权限受限，登录华为云获取权限（openeuler账号）：  
https://console.huaweicloud.com/swr/?agencyId=06060100e580109b1f11c01779b1ed20&region=cn-north-4&locale=zh-cn#/app/warehouse/warehouseMangeDetail/openeuler/om/om-data?type=ownImage

####4.运行，-e环境变量传参
docker run -e yaml_path='/root/path' -d test01

##命令行检查
####1.python脚本
python check_yaml.py -p '/root/path'  
将会输出成功与否信息