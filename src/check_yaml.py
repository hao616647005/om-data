import yaml
import traceback
import argparse


def check_yaml(path):
    try:
        data = yaml.load_all(open(path)).__next__()['users']
        print('load yaml success')
    except:
        print('load yaml failed')
        print(traceback.format_exc())
        return False
    dl = []
    for i in data:
        dl.append(i['gitee_id'])
    if len(dl) != len(set(dl)):
        print('failed: gitee id already existed')
    else:
        print('success: input contributor info')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='yaml file path.')
    parser.add_argument('--path', '-p', help='-yaml file path')
    args = parser.parse_args()
    check_yaml(args.path)
